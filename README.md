docker build -t web-server -f Dockerfile.flask .  
docker build -t reverse-proxy -f Dockerfile.nginx .  

docker run -d --net host web-server  
docker run -d --net host reverse-proxy  

